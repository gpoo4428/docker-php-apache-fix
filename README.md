# PHP w/ Apache (Fixed)

Supported tags:

* latest (based on php:apache)

Version of the official PHP image for development using mounted volumes. Uses [a hack that always makes the owner of the webroot the same as the apache user](https://github.com/kojiromike/docker-magento/blob/master/apache/start_safe_perms).